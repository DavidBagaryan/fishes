#!/bin/sh

cd ./fishes/

./manage.py flush --no-input
./manage.py makemigrations
./manage.py migrate

echo "from django.contrib.auth.models import User;" \
     "User.objects.all().delete();" \
     "User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | python manage.py shell

# run mock data generators
# all options of this command available by ./manage.py make_demo_data -h
./manage.py make_demo_data -D 1

# run tests
./manage.py test

exec "$@"
