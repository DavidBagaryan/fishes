from django.contrib import admin

from .models import *


class RackInline(admin.StackedInline):
    model = Rack
    extra = 0


class PositionInline(admin.TabularInline):
    model = Position
    extra = 0

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class StockInline(admin.StackedInline):
    model = Stock
    extra = 0


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name']}),
        ('Description', {'fields': ['description'], 'classes': ['collapse']}),
    ]
    inlines = [RackInline]

    search_fields = ('name', 'description')


@admin.register(Rack)
class RackAdmin(admin.ModelAdmin):
    list_filter = ('room',)
    inlines = [PositionInline]

    search_fields = ('height', 'width', 'id', 'room__name', 'room__description')


@admin.register(SubStock)
class SubStockAdmin(admin.ModelAdmin):
    list_filter = ('place', 'stock')
    search_fields = ('place__place_name', 'place__id', 'count', 'id')

    def get_form(self, request, obj=None, change=False, **kwargs):
        form = super().get_form(request, obj, change, **kwargs)
        selected_position = Position.objects.filter(pk=obj.position.id)

        new_qs = form.base_fields['position'].queryset.filter(is_available=1)
        new_qs |= selected_position

        form.base_fields['position'].queryset = new_qs

        return form


@admin.register(FishLine)
class FishLineAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['name']}),
        ('Description', {'fields': ['description'], 'classes': ['collapse']}),
    ]
    inlines = [StockInline]

    search_fields = ('name', 'description')
