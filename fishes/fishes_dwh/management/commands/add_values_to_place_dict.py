from django.core.management.base import BaseCommand, CommandError

from fishes_dwh.models import PlaceDictionary

default_fish_lines = 15
default_rooms = 13
default_racks_count = 7


class Command(BaseCommand):
    help = 'command to create some demo data for presentation'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        try:
            vessel_attr = {'place_name': 'vessel'}
            tank_attr = {'place_name': 'tank'}
            places = PlaceDictionary.objects

            if not places.filter(**vessel_attr).exists():
                print('created:', places.create(**vessel_attr))

            if not places.filter(**tank_attr).exists():
                print('created:', places.create(**tank_attr))

        except Exception as e:
            raise CommandError(repr(e))
