from django.core.management import call_command
from django.core.management.base import BaseCommand, CommandError

from fishes_dwh.utils import Manager

default_fish_lines = 15
default_rooms = 7
default_racks_count = 13


class Command(BaseCommand):
    help = 'command to create some demo data for presentation'

    def add_arguments(self, parser):
        parser.add_argument(
            *('--delete', '-D'),
            help='0 means do not delete existing, 1 is delete',
            choices=[0, 1],
            type=int,
            default=0,
        )
        parser.add_argument(
            *('--fish_lines', '-F'),
            help='count of fish-lines to create',
            nargs='?',
            type=int,
            default=default_fish_lines,
        )
        parser.add_argument(
            *('--rooms', '-R'),
            help='list of rooms in which the racks are placed',
            nargs='?',
            type=int,
            default=default_rooms,
        )
        parser.add_argument(
            *('--racks', '-r'),
            help='count of racks (cabinets) in which the sub-stocks can be placed',
            nargs='?',
            type=int,
            default=default_racks_count,
        )

    def handle(self, *args, **options):
        try:
            call_command('add_values_to_place_dict')

            manager = Manager(options)
            manager.create_mock_data()
        except Exception as e:
            raise CommandError(repr(e))
