from django.apps import AppConfig


class FishesDwhConfig(AppConfig):
    name = 'fishes_dwh'
