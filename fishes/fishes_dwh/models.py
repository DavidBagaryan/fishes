from django.db import models


class FishLine(models.Model):
    """information about the type of the Zebrafish"""

    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=25, db_index=True, unique=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return f'#fish-line {self.id}: "{self.name}"'


class Stock(models.Model):
    """information about a group of fish of specified fish-line"""

    class Meta:
        ordering = ['-date_of_birth']

    related_name = 'stocks'

    fish_line = models.ForeignKey(to='FishLine', related_name=related_name, on_delete=models.CASCADE)
    date_of_birth = models.DateTimeField(db_index=True)

    def __str__(self):
        return f'#stock of {self.fish_line.name}, birthday: {self.date_of_birth}'


class SubStock(models.Model):
    """
    information about the part of the given 'stock'
    in what 'position' is the SubStock placed (in a tank/vessel),
    what is the 'count' of the fish in this sub-stock
    """

    related_name = 'sub_stocks'

    position = models.OneToOneField(to='Position', related_name=related_name[:-1], on_delete=models.CASCADE)
    stock = models.ForeignKey(to='Stock', related_name=related_name, on_delete=models.CASCADE)
    place = models.ForeignKey(to='PlaceDictionary', related_name=related_name, on_delete=models.DO_NOTHING)
    count = models.IntegerField(db_index=True)

    def __str__(self):
        return f'sub-stock #ID {self.id} fishes count: {self.count}, placed in {self.place}'

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """custom update for available position"""

        prev_position = SubStock.objects.get(pk=self.pk).position if self.pk is not None else None

        if prev_position and self.position.id != prev_position.id:
            Position.objects.filter(pk=self.position.id).update(is_available=0)
            Position.objects.filter(pk=prev_position.id).update(is_available=1)

        super().save(force_insert, force_update, using, update_fields)


class PlaceDictionary(models.Model):
    """dictionary with place names for sub_stock.place column"""

    place_name = models.CharField(max_length=10, db_index=True)

    def __str__(self):
        return self.place_name


class Position(models.Model):
    """information about the available positions where sub-stocks could be stored"""

    class Meta:
        unique_together = ('rack', 'row', 'column')

    related_name = 'positions'

    rack = models.ForeignKey(to='Rack', related_name=related_name, on_delete=models.CASCADE)
    row = models.IntegerField(default=0)
    column = models.IntegerField(default=0)
    is_available = models.SmallIntegerField(db_index=True, default=1)

    def __str__(self):
        vacancy = self.is_vacant_str()
        return f'#position ID({self.id}){vacancy}, rack ID #{self.rack.id} | row: {self.row}, column: {self.column}'

    def is_vacant_str(self) -> str:
        return ' EMPTY' if self.is_available == 1 else ''


class Rack(models.Model):
    """information about racks (cabinets) in which the sub-stocks can be placed"""

    related_name = 'racks'

    MIN_RACK_HEIGHT = 3
    MIN_RACK_WIDTH = 5

    MAX_RACK_HEIGHT = 6
    MAX_RACK_WIDTH = 9

    height = models.IntegerField(default=MIN_RACK_HEIGHT)
    width = models.IntegerField(default=MIN_RACK_WIDTH)
    room = models.ForeignKey(to='Room', related_name=related_name, on_delete=models.CASCADE)

    def __str__(self):
        return f'#rack ID {self.id} | height: {self.height}, width: {self.width} | placed in room: "{self.room.name}"'


class Room(models.Model):
    """list of rooms in which the racks are placed"""

    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=25, db_index=True)
    description = models.TextField()

    def __str__(self):
        return f'#room specs: {self.name}'
