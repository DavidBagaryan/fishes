import importlib
import random
from typing import Union

from django.db import IntegrityError
from django.db.models.query import QuerySet
from django.utils.timezone import make_aware
from faker import Faker

from .models import *

FISHES_MAX_COUNT_IN_SUB_STOCK: int = 119
AVAILABLE_POSITIONS: int = 19

models_module = importlib.import_module('fishes_dwh.models')
fake = Faker()


def delete_objects_by_type(model_type: str) -> None:
    model = getattr(models_module, model_type)

    if issubclass(model, models.Model):
        print(f'{model_type}s are deleted') if model.objects.all().delete()[0] > 0 else None
    else:
        raise ValueError(f'arg {model_type} is not a subclass of model.Model')


def object_factory(model_type: str, attr: callable, return_: bool = False, **kwargs) -> Union[models.Model, None]:
    """
    simple object factory by string name type
    recursion and callable attributes to except validation errors
    """

    model = getattr(models_module, model_type)

    if issubclass(model, models.Model):
        try:
            obj = model.objects.create(**attr(**kwargs))
        except IntegrityError as ie:
            print(ie, '\nretrying create...')
            object_factory(model_type, attr, **kwargs)
        else:
            print('CREATED!', obj)
            if return_:
                return obj

    else:
        raise ValueError(f'arg {model_type} is not a subclass of model.Model')


class Manager:
    _existing_FL: list = None
    _created_stocks: list = None
    _existing_racks: list = None
    _available_rooms: list = None
    _s_stocks: list = []

    _places: QuerySet = PlaceDictionary.objects.all()

    def __init__(self, opt: dict):
        self._do_delete = opt['delete']

        self._FL_count = opt['fish_lines']
        self._rooms_count = opt['rooms']
        self._racks_count = opt['racks']

    def create_mock_data(self):
        self._delete_data()
        self._make_fish_lines()
        self._mark_fl_in_stocks()
        self._make_rooms()
        self._make_racks()
        self._make_and_place_sub_stocks()
        self._empty_some_positions(AVAILABLE_POSITIONS)

    def _delete_data(self):
        """
        method to delete all base data (FishLine, Room)
        all of the relations will be deleted cascade
        """

        if self._do_delete == 1:
            print('prepare to delete all existing data')

            delete_objects_by_type('FishLine')
            delete_objects_by_type('Room')

            print('db is empty')

    def _make_fish_lines(self):
        def _fish_line_attr() -> dict:
            return {'name': fake.name(), 'description': fake.text(max_nb_chars=50)}

        self._existing_FL = [object_factory('FishLine', _fish_line_attr, True) for _ in range(self._FL_count)]

    def _mark_fl_in_stocks(self):
        def _stock_attr() -> dict:
            return {
                'fish_line': random.choice(self._existing_FL),
                'date_of_birth': make_aware(fake.date_time_between(start_date='-7y', end_date='now'))
            }

        self._created_stocks = [object_factory('Stock', _stock_attr, True) for _ in range(len(self._existing_FL))]

    def _make_rooms(self):
        def _room_attr(number: int) -> dict:
            """mock data for creating some rooms"""

            return {'name': f'ROOM #{number} {fake.name()}', 'description': fake.text(max_nb_chars=100)}

        self._available_rooms = \
            [object_factory('Room', _room_attr, True, number=i) for i in range(1, self._rooms_count + 1)]

    def _make_racks(self):
        def _rack_attr() -> dict:
            return {
                'room': random.choice(self._available_rooms),
                'height': random.randint(Rack.MIN_RACK_HEIGHT, Rack.MAX_RACK_HEIGHT),
                'width': random.randint(Rack.MIN_RACK_WIDTH, Rack.MAX_RACK_WIDTH),
            }

        self._existing_racks = [object_factory('Rack', _rack_attr, True) for _ in range(self._racks_count)]

    def _make_and_place_sub_stocks(self):
        def _pos_attr(_rack: Rack, _height: int, _width: int) -> dict:
            return {
                'rack': _rack,
                'row': _height,
                'column': _width,
                'is_available': 0,
            }

        def _sub_stock_attr(_position: Position) -> dict:
            return {
                'stock': random.choice(self._created_stocks),
                'position': _position,
                'place': random.choice(self._places),
                'count': random.randint(1, FISHES_MAX_COUNT_IN_SUB_STOCK),
            }

        existing_racks = self._existing_racks

        for _ in range(len(existing_racks)):
            random.shuffle(existing_racks)
            rand_r = existing_racks.pop()

            for row in range(1, rand_r.height + 1):
                for cell in range(1, rand_r.width + 1):
                    new_position = object_factory('Position', _pos_attr, True, _rack=rand_r, _height=row, _width=cell)
                    if new_position:
                        self._s_stocks.append(
                            object_factory('SubStock', _sub_stock_attr, True, _position=new_position))

    def _empty_some_positions(self, emptying_positions):
        """method to emulate some positions vacant"""

        for s_stock in range(emptying_positions):
            random.shuffle(self._s_stocks)
            emptying_s_stock = self._s_stocks.pop()

            emptying_s_stock.position.is_available = 1
            emptying_s_stock.position.save()
            print('Updated:', emptying_s_stock.position)

            s_stock_id = emptying_s_stock.id
            emptying_s_stock.delete()
            print(f'SubStock deleted, ID #{s_stock_id}')
